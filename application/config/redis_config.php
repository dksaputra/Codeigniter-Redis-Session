<?php
$config['sess_cookie_name']						= 'ci_session';
$config['sess_expiration']						= 60*60*3;
$config['sess_expire_on_close']					= FALSE;
$config['sess_encrypt_cookie']					= FALSE;
$config['sess_use_database']					= TRUE;
$config['sess_table_name']						= 'tmp.ci_sessions';
$config['sess_match_ip']						= FALSE;
$config['sess_match_useragent']					= TRUE;
$config['sess_time_to_update']					= 60*60*3;

/* START: SESSION IN REDIS CONFIG */
$config['sess_store_primary']					= 'redis';
$config['sess_store_auto_backup']				= FALSE;
$config['sess_store_error_log']					= TRUE;

$config['sess_redis_master_scheme']				= 'tcp';
$config['sess_redis_master_host']				= 'localhost'; //elasticache host master
$config['sess_redis_master_port']				= 6379;
$config['sess_redis_master_db']					= 25;
$config['sess_redis_master_async']				= FALSE;
$config['sess_redis_master_persistent']			= FALSE;
$config['sess_redis_master_timeout']			= 5;
$config['sess_redis_master_throw_errors']		= FALSE;
$config['sess_redis_master_read_write_timeout']	= 5;

$config['sess_redis_slave_scheme']				= 'tcp';
$config['sess_redis_slave_host']				= 'localhost'; //elasticache host slave
$config['sess_redis_slave_port']				= 6379;
$config['sess_redis_slave_db']					= 25;
$config['sess_redis_slave_async']				= FALSE;
$config['sess_redis_slave_persistent']			= FALSE;
$config['sess_redis_slave_timeout']				= 5;
$config['sess_redis_slave_throw_errors']		= FALSE;
$config['sess_redis_slave_read_write_timeout']	= 5;
/* END: SESSION IN REDIS CONFIG */