## Instruction
1. Add and copy folder /system/libraries/Predis, /system/libraries/Predis.php and /system/libraries/Session.php 
2. Add and copy folder /assets/json/session
3. Add or copy folder /application/config/redis_config.php
4. Setting kernel linux TCP
	- echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse
	- echo 1 > /proc/sys/net/ipv4/tcp_tw_recycle
5. Create AWS elasticache (redis) 1 master and 1 slave
	- bb-redis-001.photjt.0001.apse1.cache.amazonaws.com (master)
	- bb-redis-002.photjt.0001.apse1.cache.amazonaws.com (slave)
	
## Redis monitoring
1. Using new relic plugin and aws
2. ./redis-cli -h [host] -p [port]